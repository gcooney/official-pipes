'''
Utils
'''
from official_build.api.bitbucket.base import BitbucketApiService


def get_latest_git_tag(repository_path):
    bitbucket_api_service = BitbucketApiService()
    # Get latest tag
    return bitbucket_api_service.get_latest_tag(repository_path)
